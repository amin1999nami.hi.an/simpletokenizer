﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.CSharp.RuntimeBinder;

namespace Tokenizer
{
    class Program
    {
        public static string code { get; set; }

        public static  int index { get; set; }
        public static bool End { get; set; }


        static void Main(string[] args)
        {   
            StreamReader stream = new StreamReader(@"C:\Users\Amin N\Desktop\Compiler\Tokenizer\Tokenizer\code.txt");
            code = stream.ReadToEnd().ToString();


            


            index = 0;
            End = false;

            Tokenize(code);
           




        }

        static void Tokenize(string code)
        {
            List<string> output = new List<string>();

            while (!End)
            {
                output.Add(NextToken());
            }


            Show(output);

        }

        static string NextToken()
        {
            List<char> TokenMaker;

            TokenMaker = new List<char>();
            char Character;
            bool Equality=false;

            while (true)
            {
                Character = GetChar();
                index++;
                

                if (Character == ' ')
                {
                    string s = new string(TokenMaker.ToArray());
                    
                    return s;
                }
                
                if (Character == ';'||
                    Character == ':'||
                    Character == ')'||
                    Character == '('||
                    Character == '{'||
                    Character == '}'||
                    Character == '+'||
                    Character == '/'||
                    Character == '-'||
                    Character == '*'||
                    Character == ',')
                {
                    if (TokenMaker.Count == 0)
                    {
                        TokenMaker.Add(Character);

                        string s = new string(TokenMaker.ToArray());


                        return s;
                    }

                    else
                    {
                        UnGetChar();

                        string s = new string(TokenMaker.ToArray());


                        return s;
                    }
                }


                if (Character == '='||
                    Character == '>'||
                    Character == '<'||
                    Character == '!')
                {
                    if (TokenMaker.Count == 0)
                    {
                        TokenMaker.Add(Character);
                        Equality = true;
                    }

                    else
                    {
                        if (TokenMaker.Count == 1 && Character=='='&& Equality)
                        {
                            TokenMaker.Add(Character);

                            string s = new string(TokenMaker.ToArray());

                            Equality = false;

                            return s;
                        }
                        else
                        {
                            UnGetChar();
                            string s = new string(TokenMaker.ToArray());

                            return s;
                        }

                    }
                }

               

                

                else
                {
                    TokenMaker.Add(Character);
                }

                if (index == code.Length - 1)
                {
                    End = true;

                    string s = new string(TokenMaker.ToArray());

                    return s;
                }
            }

        }

        static char GetChar()
        {
            return code[index];
        }

        static void UnGetChar()
        {
            index--;
        }

        static void Show(List<String> ShowThis)
        {
            foreach (var item in ShowThis)
            {   
                Console.WriteLine(item);
            }
        }

    }
}
